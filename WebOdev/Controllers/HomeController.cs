﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebOdev.Models;

namespace WebOdev.Controllers
{
    
    public class HomeController : Controller
    {
        WebDbEntities ent = new WebDbEntities();
        
        public ActionResult Index()
        {
            // slider icin
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.slider=ent.Slider.Where(x=>(x.BaslangicTarih<=DateTime.Now && x.BitisTarih>DateTime.Now)).ToList();
            obj.haberlerim = ent.Haber.ToList();
            obj.haberformum = new HaberFormu();
            string gcc = "SPOR";
            
            obj.spor= ent.Haber.Where(x => x.Kategori.ToUpper() == gcc).Take(2).ToList();

            string gcc1 = "ekonomi";
            
            obj.ekonomi = ent.Haber.Where(x => x.Kategori.ToUpper() == gcc1.ToUpper()).Take(2).ToList();

            string gcc2 = "magazin";
            obj.magazin = ent.Haber.Where(x => x.Kategori.ToUpper() == gcc2.ToUpper()).ToList();

            //var gecici=obj.haberlerim.Select(x=>x.HaberIcerik);

            return View("Index",obj);
        }

      


        public ActionResult HaberAltSayfa(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haberim = ent.Haber.Find(id);
            if (haberim == null)
            {
                return HttpNotFound();
            }
            
            return View(haberim);
        }

      


        [HttpPost]
        public ActionResult HaberGonder(AnasayfaDTO hbrform)
        {
            try
            {
                HaberFormu haberform = new HaberFormu();
                haberform.AdSoyad = hbrform.haberformum.AdSoyad;
                haberform.Telefon = hbrform.haberformum.Telefon;
                haberform.Email = hbrform.haberformum.Email;
                haberform.HaberYazisi = hbrform.haberformum.HaberYazisi;
                ent.HaberFormu.Add(haberform);
                ent.SaveChanges();
                TempData["Mesaj"] = "Haberiniz Başarıyla Gönderildi.";
                return Index();
            }
            catch(Exception ex)
            {
                throw new Exception("Hata Oluştu.Daha Sonra Tekrar Deneyiniz.");
            }
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult SonDakika(string arama)
        {
            // haberleri listeleme
            AnasayfaDTO objem = new AnasayfaDTO();
            objem.sonDakika= ent.Haber.OrderByDescending(x => x.HaberID).ToList();
            // arama

            

            if(!String.IsNullOrEmpty(arama))
            {
                objem.sonDakika = ent.Haber.Where(s => s.HaberBaslik.ToUpper().Contains(arama.ToUpper())).ToList();
            }
            

            return View(objem);
        }
        public ActionResult Gazeteler()
        {
            return View();
        }
        public ActionResult Spor(string arama)
        {
            string gcc = "SPOR";
            AnasayfaDTO objSpr = new AnasayfaDTO();
            objSpr.spor = ent.Haber.Where(x => x.Kategori.ToUpper() == gcc).ToList();


            // arama
            if (!String.IsNullOrEmpty(arama))
            {
                objSpr.spor = objSpr.spor.Where(s => s.HaberBaslik.ToUpper().Contains(arama.ToUpper())).ToList();
            }

            return View(objSpr);
        }
        public ActionResult Ekonomi(string arama)
        {
            string gcc = "ekonomi";
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.ekonomi = ent.Haber.Where(x => x.Kategori.ToUpper() == gcc.ToUpper()).ToList();


            // arama
            if (!String.IsNullOrEmpty(arama))
            {
                obj.ekonomi = obj.ekonomi.Where(s => s.HaberBaslik.ToUpper().Contains(arama.ToUpper())).ToList();
            }

            return View(obj);
            
        }
        public ActionResult Magazin(string arama)
        {
            string gcc = "magazin";
            AnasayfaDTO obj = new AnasayfaDTO();
            obj.magazin = ent.Haber.Where(x => x.Kategori.ToUpper() == gcc.ToUpper()).ToList();


            // arama
            if (!String.IsNullOrEmpty(arama))
            {
                obj.magazin = obj.magazin.Where(s => s.HaberBaslik.ToUpper().Contains(arama.ToUpper())).ToList();
            }

            return View(obj);
        }
    }

    public class AnasayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Haber> haberlerim { get; set; }
        public List<Haber> sonDakika { get; set; }
        public List<Haber> spor { get; set; }
        public List<Haber> ekonomi { get; set; }
        public List<Haber> magazin { get; set; }

        public HaberFormu haberformum { get; set; }

    }
}