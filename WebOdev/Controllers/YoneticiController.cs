﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebOdev.Models;

namespace WebOdev.Controllers
{
    [Authorize(Users ="abc@hotmail.com")]
    public class YoneticiController : Controller
    {
        private WebDbEntities db = new WebDbEntities();
        // GET: Yonetici
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SliderIndex()
        {
            var sliders = db.Slider.ToList();
            return View(sliders);
        }
        
        public ActionResult SliderDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }
        public ActionResult SliderCreate()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SliderCreate([Bind(Include = "ID,SliderText,BaslangicTarih,BitisTarih,ResimURL")] Slider slider)
        {
            if (ModelState.IsValid)
            {
                db.Slider.Add(slider);
                db.SaveChanges();
                return RedirectToAction("SliderIndex");
            }

            return View(slider);
        }

        public ActionResult SliderEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SliderEdit([Bind(Include = "ID,SliderText,BaslangicTarih,BitisTarih,ResimURL")] Slider slider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(slider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("SliderIndex");
            }
            return View(slider);
        }

        public ActionResult SliderDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        [HttpPost, ActionName("SliderDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult SliderDeleteConfirmed(int id)
        {
            Slider slider = db.Slider.Find(id);
            db.Slider.Remove(slider);
            db.SaveChanges();
            return RedirectToAction("SliderIndex");
        }

        public ActionResult HaberFormuIndex()
        {
            return View(db.HaberFormu.ToList());
        }

        public ActionResult HaberFormuDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HaberFormu haberFormu = db.HaberFormu.Find(id);
            if (haberFormu == null)
            {
                return HttpNotFound();
            }
            return View(haberFormu);
        }

        public ActionResult HaberFormuDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HaberFormu haberFormu = db.HaberFormu.Find(id);
            if (haberFormu == null)
            {
                return HttpNotFound();
            }
            return View(haberFormu);
        }
        [HttpPost, ActionName("HaberFormuDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult HaberFormuDeleteConfirmed(int id)
        {
            HaberFormu haberFormu = db.HaberFormu.Find(id);
            db.HaberFormu.Remove(haberFormu);
            db.SaveChanges();
            return RedirectToAction("HaberFormuIndex");
        }

        // haberler

        public ActionResult HaberIndex()
        {
            return View(db.Haber.ToList());
        }

        public ActionResult HaberDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haber.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }
        public ActionResult HaberCreate()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HaberCreate([Bind(Include = "HaberID,HaberBaslik,HaberIcerik,HaberTarih,HaberYer,KucukResim,Resim1URL,Resim2URL,Kategori")] Haber haber)
        {
            if (ModelState.IsValid)
            {
                db.Haber.Add(haber);
                db.SaveChanges();
                return RedirectToAction("HaberIndex");
            }

            return View(haber);
        }

        public ActionResult HaberEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haber.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HaberEdit([Bind(Include = "HaberID,HaberBaslik,HaberIcerik,HaberTarih,HaberYer,KucukResim,Resim1URL,Resim2URL,Kategori")] Haber haber)
        {
            if (ModelState.IsValid)
            {
                db.Entry(haber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("HaberIndex");
            }
            return View(haber);
        }

        public ActionResult HaberDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haber.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }

        [HttpPost, ActionName("HaberDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult HaberDeleteConfirmed(int id)
        {
            Haber haber = db.Haber.Find(id);
            db.Haber.Remove(haber);
            db.SaveChanges();
            return RedirectToAction("HaberIndex");
        }

    }
}